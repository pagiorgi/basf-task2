terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.82.0"
    }
  }
}

provider "azurerm" {
   features {}
}
resource "azurerm_resource_group" "task2" {
    name = "task2-rg"
    location = "West US 3" 
}
resource "azurerm_virtual_network" "task2-vnet" {
  name                = "vnet01"
  location            = azurerm_resource_group.task2.location
  resource_group_name = azurerm_resource_group.task2.name
  address_space       = ["10.0.0.0/16"]
  #dns_servers         = ["10.0.0.4", "10.0.0.5"]  optional

  subnet {
    name           = "subnet1"
    address_prefix = "10.0.1.0/24"
  }
}
resource "azurerm_subnet" "task2-subnet" {
    name                 = "snet01"
    resource_group_name  = azurerm_resource_group.task2.name
    virtual_network_name = azurerm_virtual_network.task2-vnet.name
    address_prefixes     = ["10.0.1.0/24"]
}
# privete endpoint 
resource "azurerm_private_endpoint" "task2-pe01" {
  name                = "pe-openai-we"
  location            = azurerm_resource_group.task2.location
  resource_group_name = azurerm_resource_group.task2.name
  subnet_id           = azurerm_subnet.task2-subnet.id


  private_service_connection {
    name                           = "pe-openai-we"
    private_connection_resource_id = azurerm_cognitive_account.task2.id
    subresource_names              = ["account"]
    is_manual_connection           = false
  }

    private_dns_zone_group {
    name                 = "default"
    private_dns_zone_ids = [azurerm_private_dns_zone.etask2.id]
  }
}

resource "azurerm_private_dns_zone" "task2" {
  name                = "privatelink.openai.azure.com"
  resource_group_name = azurerm_resource_group.task2.name
}

resource "azurerm_private_dns_zone_virtual_network_link" "task2" {
  name                  = "task2-vnet-link"
  resource_group_name   = azurerm_resource_group.task2.name
  private_dns_zone_name = azurerm_private_dns_zone.task2.name
  virtual_network_id    = azurerm_virtual_network.task2-vnet.id
}


# Create an Azure Cognitive Service Account
resource "azurerm_cognitive_account" "task2" {
  name                = "task2-ca"
  location            = azurerm_resource_group.task2.location
  resource_group_name = azurerm_resource_group.task2.name
  kind                = "OpenAI"
  sku_name            = "S0"
  public_network_access_enabled = false #sets public network access enabled = false, meaning that you will only be able to reach this service via the private endpoint and not the public address of the service
  custom_subdomain_name = "task2-openai2"
}
# Use Customer Managed Key for the Cognitive Service Account

resource "azurerm_cognitive_account_customer_managed_key" "task2-cmk" {
  cognitive_account_id = azurerm_cognitive_account.main.id
  key_source           = "Microsoft.KeyVault"
  key_vault_id         = data.azurerm_key_vault.task2.id
  key_vault_key_id     = data.azurerm_key_vault_keys.task2["keyName"].id
}

resource "azurerm_cognitive_deployment" "task2" {
  name                 = "GPT35"
  cognitive_account_id = azurerm_cognitive_account.task2.id
  model {
    format  = "OpenAI"
    name    = "gpt-35-turbo"
    version = "1"
  }
  scale {
    type = "Standard"
  }
}
# Output the keys and endpoint of the Cognitive Service Account

output "cognitive_account_key1" {
  value = azurerm_cognitive_account.task2.account_keys[0]
}

output "cognitive_account_key2" {
  value = azurerm_cognitive_account.task2.account_keys[1]
}

output "cognitive_account_endpoint" {
  value = azurerm_cognitive_account.task2.endpoint
}
