# basf-task2


## Resources Description

**resource group** : A resource group is a container that holds related resources for an Azure solution. The resource group can include all the resources for the solution, or only those resources that you want to manage as a group.

**virtual_network** : This Terraform resource block below covers a virtual network (VNet) in Azure: that’s essential if you would like to create a private network. An Azure resource, like a virtual machine, which is deployed in this VNet in my example, can communicate with other resources within this VNet securely.


**subnet**: The next important resource block is the subnet. Subnets are used to divide the network into several, smaller parts. For instance, the VNet could contain for example a subnet A and a subnet B. I’ll create just one subnet, which is named “internal”. The value of the “address_prefixes” attribute is “10.0.2.0/24”. According to the calculation, there are 256 possible IP addresses for this subnet

**Private endpoint**:Azure Private Endpoint is a network interface that connects you privately and securely to a service powered by Azure Private Link. Private Endpoint uses a private IP address from your VNet, effectively bringing the service into your VNet. The service could be an Azure service such as Azure Storage, SQL, etc. or your own Private Link Service.Private endpoint

**cognitive service**: Azure Cognitive Services is a set of cloud-based APIs that you can use in AI applications and data flows. It provides pretrained models that are ready to use in your applications, requiring no data and no model training on your part.


Terraform init :

 ![alt tag](./images/terraformini.PNG)

